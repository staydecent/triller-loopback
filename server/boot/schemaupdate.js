let async = require('async')

module.exports = function (app, cb) {
  let datasources = Object.keys(app.dataSources)

  async.eachSeries(datasources, function (dsName, cb) {
    let ds = app.dataSources[dsName]

    ds.isActual(function (_, actual) {
      if (actual) {
        console.log('datasource', dsName, 'is up to date')
        return cb()
      }
      ds.autoupdate(function () {
        console.log('datasource', dsName, 'updated')
        cb()
      })
    })
  }, cb)
}
