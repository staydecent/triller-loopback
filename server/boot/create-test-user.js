'use strict'

module.exports = function (app) {
  app.dataSources.localpsql.automigrate('Person', function (err) {
    if (err) throw err

    app.models.Person.create([{
      firstname: 'Adrian',
      lastname: 'Unger',
      email: 'dev@staydecent.ca',
      password: 'neatneat'
    }], function (err, people) {
      if (err) throw err

      console.log('Models created: \n', people)
    })
  })
}
