'use strict'

let path = require('path')
let fs = require('fs')

module.exports = function (app) {
  app.use(function (req, res, next) {
    if (req.path.indexOf('explorer') !== -1 ||
      req.path.indexOf('api') !== -1) {
      next()
    } else {
      let potentialFile = path.join(__dirname, '../../client', req.path)
      try {
        fs.accessSync(potentialFile, fs.F_OK)
        res.sendFile(potentialFile)
      } catch (e) {
        res.sendFile(path.join(__dirname, '../../client', 'index.html'))
      }
    }
  })
}
