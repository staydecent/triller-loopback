import app from '../app'

app.component('allProjects', {
  template: `
    <nav class="level">
      <div class="level-left">
      </div>

      <div class="level-right">
        <p class="level-item"><a>Add Team</a></p>
        <p class="level-item"><a class="button is-primary" ng-click="scope.showAddProjectModal($event)">Add Project</a></p>
      </div>
    </nav>

    <section ng-repeat="team in scope.teams">
      <h2 class=title>{{team.name}}</h2>
      <div class="columns is-mobile">
        <div class="column is-one-quarter" ng-repeat="project in scope.projects | filter: {teamId: team.id}">
          <project-card project="project"></project-card>
        </div>
      </div>
    </section>
  `,
  controller: AllProjectsController,
  controllerAs: 'scope',
  bindings: {
  }
})

AllProjectsController.$inject = [
  '$q',
  '$rootScope',
  'authService',
  'Person',
  'Team'
]

function AllProjectsController (
  $q,
  $rootScope,
  authService,
  Person,
  Team
) {
  let scope = this

  scope.showAddProjectModal = (ev) => {
    ev.preventDefault()
    const modalScope = {
      key: 'add-project',
      teams: scope.teams,
      selectedTeam: scope.teams[0],
      onSubmit: function () {
        return $q((resolve, reject) => Team.projects
          .create({id: this.selectedTeam.id}, {name: this.projectName})
          .$promise
          .then((project) => {
            console.warn('onSubmit', project)
            scope.projects.push(project)
            resolve(true)
          })
          .catch((err) => {
            console.error('onSubmit', err)
            reject(false)
          })
        )
      }
    }
    $rootScope.$emit('show-modal', modalScope)
  }

  scope.$onInit = () => {
    authService
      .currentUser()
      .then((user) => Person.teams({id: user.id}).$promise)
      .then((teams) => {
        scope.teams = teams
        return $q.all(teams.map(({id}) => Team.projects({id}).$promise))
      })
      .then((projects) => {
        scope.projects = projects.reduce((arr, p) => arr.concat(p), [])
        console.warn('scope.projects', scope)
      })
  }
}
