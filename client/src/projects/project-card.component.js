import app from '../app'

app.component('projectCard', {
  template: `
    <div class="card">
      <header class="card-header">
        <p class="card-header-title">
          {{scope.project.name}}
        </p>
      </header>
      <div class="card-content">
        <div class="content">
          <link-to path="/projects/{{scope.project.id}}" data="{project: scope.project}">
            {{scope.project.name}}
          </link-to>
        </div>
      </div>
    </div>
  `,
  controller: ProjectCardController,
  controllerAs: 'scope',
  bindings: {
    project: '<'
  }
})

ProjectCardController.$inject = [
]

function ProjectCardController () {
}
