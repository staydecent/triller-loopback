import app from '../app'

app.component('addProject', {
  template: `
    <label class="label">Name</label>
    <p class="control">
      <input class="input" type="text" name="name" ng-model="$ctrl.parent.projectName" placeholder="Name your project">
    </p>

    <label class="label">Team</label>
    <p class="control">
      <span class="select">
        <select ng-options="team as team.name for team in $ctrl.parent.teams track by team.id" ng-model="$ctrl.parent.selectedTeam"></select>
      </span>
    </p>
  `,
  require: {
    parent: '^^modalOverlay'
  }
})
