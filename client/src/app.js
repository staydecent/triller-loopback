/* global angular */
const app = angular.module('triller', [
  'lbServices'
])

/**
 * Our app is built with a sort-of 'front controller' pattern. Where
 * MainController is called on every page request and handles authorization.
 *
 * Authorization data is passed down to components in `index.html` which
 * are enabled or disabled based on a simple router.
 */
app.controller('MainController', MainController)

MainController.$inject = ['$rootScope', '$window', 'authService']

function MainController ($rootScope, $window, authService) {
  let scope = this
  scope.loginForm = {email: '', password: ''}
  scope.login = () => {
    if (scope.loginForm.email && scope.loginForm.password) {
      return authService
        .login(scope.loginForm.email, scope.loginForm.password)
        .then((user) => {
          scope.user = user
          scope.showLoginForm = false
          $rootScope.$emit('login')
          return true
        })
        .catch(() => {
          scope.loginForm.failed = true
          return false
        })
        .finally(() => {
          scope.loginForm.loading = false
        })
    } else {
      scope.loginForm.loading = false
    }
  }

  authService
    .currentUser()
    .then((user) => {
      scope.user = user
    })
    .catch(() => {})

  $rootScope.$on('unauthorizedView', (ev, path) => {
    console.error('unauthorizedView', path)
    scope.showLoginForm = true
  })

  $rootScope.$on('changeState', () => {
    scope.showLoginForm = false
  })
}

export default app
