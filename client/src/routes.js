import app from './app'

app.config(defineRoutes)
defineRoutes.$inject = ['$routeProvider', '$locationProvider']

function defineRoutes ($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true)
  $routeProvider
    .when('/projects', {
      templateUrl: 'public/projects/projects.html',
      controller: 'ProjectsController as scope'
    })
    .when('/projects/:projectId', {
      templateUrl: 'public/projects/project.html',
      controller: 'ProjectController as scope'
    })
}

app.run(['$rootScope', '$window', ($rootScope, $window) => {
  $rootScope.$on('$routeChangeStart', (event, next) => {
    // redirect to login page if not logged in
    if (next.authenticate && !$rootScope.currentUser) {
      event.preventDefault() // prevent current page from loading
      $window.location.href = '/forbidden'
    }
  })
}])
