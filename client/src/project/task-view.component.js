import app from '../app'

app.component('taskView', {
  template: `
    <div class="modal" ng-class="{'is-active': $ctrl.task}">
      <div class="modal-background" ng-click="$ctrl.task = undefined"></div>
    </div>
    <div id="task-view" class="has-shadow" ng-show="$ctrl.task" ng-style="$ctrl.styles">
      <h1 class=title>{{$ctrl.task.title}}</h1>
    </div>
  `,
  controller: TaskViewController,
  bindings: {
    task: '='
  }
})

TaskViewController.$inject = ['$window', '$document', '$element']

function TaskViewController ($window, $document, $element) {
  let scope = this

  scope.styles = {
    position: 'absolute',
    background: '#fff',
    height: '100%',
    'min-width': '35%',
    'z-index': 2000,
    top: 0,
    right: 0
  }

  scope.$postLink = () => {
    console.warn('TaskViewController', $element[0])
    const height = $window.innerHeight + 'px'
    $document[0].body.style.height = height
    let el = $document[0].getElementById('task-view')
    el.style.height = height
  }
}
