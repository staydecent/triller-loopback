import app from '../app'

app.component('editProject', {
  template: `
    <label class="label">Name</label>
    <p class="control">
      <input class="input" type="text" name="name" ng-model="$ctrl.parent.projectName" placeholder="Name your project">
    </p>

    <label class="label">Delete this Project?</label>
    <p class="control">
      <span class="select">
        <select ng-options="item for item in ['YES', 'NO']" ng-model="$ctrl.deleteSelection"></select>
      </span>
      <a
        class="button is-danger"
        ng-class="{'is-loading': $ctrl.parent.loading}"
        ng-if="$ctrl.deleteSelection === 'YES'"
        ng-click="$ctrl.parent.deleteProject()"
      >Delete</a>
    </p>
  `,
  require: {
    parent: '^^modalOverlay'
  }
})
