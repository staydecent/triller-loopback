import app from '../app'

app.component('addTask', {
  template: `
    <label class="label">Title</label>
    <p class="control">
      <input class="input" type="text" name="title" ng-model="$ctrl.parent.task.title" placeholder="" autofocus>
    </p>

    <div class=columns>
      <div class="column is-2">
        <label class="label">Priority</label>
        <p class="control">
          <span class="select is-medium">
            <select ng-options="item for item in [1, 2, 3, 4]" ng-model="$ctrl.parent.task.priority"></select>
          </span>
        </p>
      </div>
      <div class="column is-2">
        <label class="label">Estimate</label>
        <p class="control">
          <span class="select is-medium">
            <select ng-options="item for item in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]" ng-model="$ctrl.parent.task.days"></select>
          </span>
        </p>
      </div>
    </div>
  `,
  require: {
    parent: '^^modalOverlay'
  }
})
