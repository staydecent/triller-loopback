import dragula from 'dragula'
import find from 'ramda/src/find'
import filter from 'ramda/src/filter'
import any from 'ramda/src/any'
import reduce from 'ramda/src/reduce'
import propEq from 'ramda/src/propEq'

import app from '../app'

app.component('projectBoard', {
  template: `
    <div class="container">
      <nav class="level">
        <div class="level-left">
          <div class="level-item"><h2 class="title">{{scope.project.name}}</h2></div>
          <div class="level-item" ng-if="scope.filters.length">
            Filters:
            <span class="tag is-white" ng-repeat="filter in scope.filters">
              {{filter[0]}}: {{filter[1]}} <button class="delete is-small" ng-click="scope.filters.splice($index, 1)"></button>
            </span>
          </div>
        </div>

        <div class="level-right">
          <p class="level-item"><a href="#" ng-click="scope.showEditProjectModal($event)">Edit Project</a></p>
          <p class="level-item"><a href="#" ng-click="scope.showAddTaskModal($event)" class="button is-primary">Add Task</a></p>
        </div>
      </nav>
    </div>

    <section style="overflow-x: scroll; overflow-y: hidden">
      <div class="columns">
        <div class="column is-2" ng-repeat="col in scope.columns">
          <nav class="panel">
            <p class="panel-heading">
              {{col}} <small>({{scope.getEstimate(col)}} days)</small>
            </p>
            <div class="panel-block" data-col="{{col}}" style="display: block; max-height: 560px; overflow: scroll">
              <div class="card" data-id="{{task.id}}" ng-repeat="task in scope.tasks | filter: scope.filterTasks(col)" style="margin-bottom: .75rem;">
                <div class="card-content" style="padding: 1rem;">
                  <div class="content">
                    {{task.title}}
                  </div>
                </div>
                <footer class="card-footer" style="font-size: 0.8rem;">
                  <a class="card-footer-item" ng-click="scope.parent.ctrl.selectedTask = task">Open</a>
                  <a class="card-footer-item" ng-click="scope.showEditTaskModal($event, task)">Edit</a>
                  <span class="card-footer-item"><span class="icon is-small"><i class="fa fa-calendar"></i></span>&nbsp;{{task.days}} days</span>
                  <a class="card-footer-item" ng-click="scope.addFilter(['priority', task.priority])"><span class="button is-outlined is-small" ng-class="scope.tagClass(task)">P{{task.priority}}</span></a>
                </footer>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </section>
  `,
  controller: ProjectBoardController,
  controllerAs: 'scope',
  require: {
    parent: '^^defineRoute'
  },
  bindings: {
    user: '<'
  }
})

ProjectBoardController.$inject = [
  '$window',
  '$document',
  '$rootScope',
  '$timeout',
  '$element',
  '$q',
  'authService',
  'Project'
]

function ProjectBoardController (
  $window,
  $document,
  $rootScope,
  $timeout,
  $element,
  $q,
  authService,
  Project
) {
  let scope = this

  $document.find('body').addClass('project-board')

  scope.loading = true
  scope.filters = []
  scope.columns = [
    'On Hold',
    'Backlog',
    'Bugs',
    'In Progress',
    'Review',
    'Done'
  ]

  scope.$onInit = () => {
    authService
      .currentUser()
      .then((user) => {
        if (scope.parent && scope.parent.data) {
          return $q.resolve(scope.parent.data.project)
        } else {
          return Project.findById({id: scope.parent.params[0]}).$promise
        }
      })
      .then((project) => {
        scope.project = project
        return Project.tasks({id: project.id}).$promise
      })
      .then((tasks) => {
        scope.tasks = tasks
      })
      .finally(() => {
        scope.loading = false
        console.log('ProjectBoardController', scope)
      })
  }

  scope.$postLink = () => {
    $timeout(() => {
      let drake = dragula()
      const panels = $element[0].querySelectorAll('.panel-block')
      for (var x = 0; x < panels.length; x++) {
        drake.containers.push(panels[x])
      }

      drake.on('drop', (el, target, source) => {
        // Update column if changed
        if (target !== source) {
          let task = find(propEq('id', parseInt(el.getAttribute('data-id'), 10)))(scope.tasks)
          task.column = target.getAttribute('data-col')
          task.$save()
        }

        // Update order for all tasks in this column
        const children = el.parentNode.children
        for (let x = 0; x < children.length; x++) {
          let child = children[x]
          let task = find(propEq('id', parseInt(child.getAttribute('data-id'), 10)))(scope.tasks)
          task.order = x
        }
      })
    }, 0)
  }

  scope.tagClass = (task) => {
    switch (task.priority) {
      case 4: return {'is-light': true}
      case 3: return {'is-dark': true}
      case 2: return {'is-warning': true}
      case 1: return {'is-danger': true}
    }
  }

  scope.getEstimate = (column) => {
    if (!scope.tasks) return 0
    const tasks = filter(propEq('column', column), scope.tasks)
    if (tasks && tasks.length) {
      const total = reduce((acc, t) => acc + (t.days || 0), 0, tasks)
      return total
    }
    return 0
  }

  scope.addFilter = (filter) =>
    !any((f) => f[0] === filter[0] && f[1] === filter[1])(scope.filters)
      ? scope.filters.push(filter)
      : null

  scope.filterTasks = (col) => (task) =>
    !propEq('column', col)(task)
      ? false
      : scope.filters.length
        ? any((filter) => propEq(filter[0], filter[1])(task))(scope.filters)
        : true

  scope.showEditProjectModal = (ev) => {
    ev.preventDefault()
    $rootScope.$emit('show-modal', {
      key: 'edit-project',
      projectName: scope.project.name,
      deleteProject: function () {
        this.loading = true
        scope.project.$delete((resp) => {
          this.loading = false
          this.active = false
          $window.location.href = '/projects'
        })
      },
      onSubmit: function () {
        scope.project.name = this.projectName
        return scope.project.$save()
      }
    })
  }

  scope.showEditTaskModal = (ev, task) => {
    ev.preventDefault()
    $rootScope.$emit('show-modal', {
      key: 'edit-task',
      task: task,
      onSubmit: function () {
        return this.task.$save()
      }
    })
  }

  scope.showAddTaskModal = (ev) => {
    ev.preventDefault()
    $rootScope.$emit('show-modal', {
      key: 'add-task',
      task: {title: '', priority: 4, days: 1},
      onSubmit: function () {
        return $q((resolve, reject) => Project.tasks
          .create({
            id: scope.project.id
          }, {
            title: this.task.title,
            priority: this.task.priority,
            days: this.task.days,
            personId: scope.user.id
          })
          .$promise
          .then((task) => {
            console.warn('task', task, scope)
            scope.tasks.push(task)
            resolve(true)
          })
          .catch((err) => {
            console.error('onSubmit', err)
            reject(false)
          })
        )
      }
    })
  }
}
