import app from '../app'

const EVENT_NAME = 'show-modal'

app.component('modalOverlay', {
  template: `
    <div class="modal" ng-class="{'is-active': scope.active}">
      <div class="modal-background" ng-click="scope.cancel()"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">{{scope.title}}</p>
          <button class="delete" ng-click="scope.cancel()"></button>
        </header>
        <section class="modal-card-body">
          <div ng-transclude></div>
        </section>
        <footer class="modal-card-foot">
          <a
            class="button is-primary"
            ng-class="{'is-loading': scope.loading}"
            ng-if="scope.onSubmit"
            ng-click="scope.submit()"
          >
            Submit
          </a>
          <a class="button" ng-click="scope.cancel()">Cancel</a>
        </footer>
      </div>
    </div>
  `,
  transclude: true,
  controller: ModalOverlayController,
  controllerAs: 'scope',
  bindings: {
    key: '@',
    title: '@',
    loading: '=?',
    onSubmit: '&'
  }
})

ModalOverlayController.$inject = ['$rootScope', '$element']

function ModalOverlayController ($rootScope, $element) {
  let scope = this

  scope.active = false
  scope.inputs = null

  scope.submit = () => {
    scope.loading = true
    scope
      .onSubmit()
      .then((result) => {
        if (result) {
          scope.active = false
          scope.loading = false
        }
      })
  }

  scope.cancel = () => {
    scope.loading = false
    scope.active = false
  }

  const handleEnter = (ev) => {
    if (ev.keyCode === 13) {
      scope.submit()
    }
  }

  scope.$onDestroy = () => {
    if (scope.inputs) {
      scope.inputs.off(handleEnter)
    }
  }

  $rootScope.$on(EVENT_NAME, (ev, data) => {
    if (data && data.key === scope.key) {
      scope.active = true
      // Can override bindings
      const keys = Object.keys(data)
      for (var x = 0; x < keys.length; x++) {
        if (keys[x] !== 'key') {
          scope[keys[x]] = data[keys[x]]
        }
      }
      if (!scope.inputs) {
        scope.inputs = $element.find('input')
        scope.inputs.on('keyup', handleEnter)
      }
      console.log(EVENT_NAME, scope)
    }
  })
}
