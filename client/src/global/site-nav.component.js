import app from '../app'

app.component('siteNav', {
  template: `
    <nav class="nav has-shadow">
      <div class="container">
        <div class="nav-left">
          <a class="nav-item">
            <img src="http://bulma.io/images/bulma-logo.png" alt="Bulma logo">
          </a>
          <link-to
            path="/"
            class-name="nav-item is-tab is-hidden-mobile"
            is-active="{{scope.isActive('/')}}"
          >
            Home
          </link-to>
          <link-to
            path="/projects"
            class-name="nav-item is-tab is-hidden-mobile"
            is-active="{{scope.isActive('/projects')}}"
          >
            Projects
          </link-to>
        </div>
        <span class="nav-toggle">
          <span></span>
          <span></span>
          <span></span>
        </span>
        <div class="nav-right nav-menu">
          <a class="nav-item is-tab" ng-if="scope.user">
            <figure class="image is-16x16" style="margin-right: 8px;">
              <img src="http://bulma.io/images/jgthms.png">
            </figure>
            {{scope.fullname()}}
          </a>
          <a class="nav-item is-tab" ng-if="scope.user" ng-click="scope.logout()">Log Out</a>
          <a class="nav-item is-tab" ng-if="!scope.user" ng-click="scope.login()">Log In</a>
        </div>
      </div>
    </nav>
  `,
  controller: SiteNavController,
  controllerAs: 'scope',
  bindings: {
    user: '<'
  }
})

SiteNavController.$inject = ['$window', '$rootScope', 'authService']

function SiteNavController ($window, $rootScope, authService) {
  let scope = this

  scope.isActive = (path) =>
    $window.location.pathname === path

  scope.login = () =>
    $rootScope.$emit('show-modal', {key: 'log-in'})

  scope.logout = () =>
    authService.logout() && $window.location.reload()

  scope.fullname = () =>
    `${scope.user.firstname} ${scope.user.lastname}`

  scope.$onInit = () => {
    console.log('SiteNavController', scope)
  }
}
