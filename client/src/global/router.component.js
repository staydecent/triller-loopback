import app from '../app'

const EVENT_NAME = 'changeState'

app.component('defineRoute', {
  template: `
    <div ng-transclude ng-if=scope.show></div>
  `,
  controller: DefineRouteController,
  controllerAs: 'scope',
  transclude: true,
  bindings: {
    path: '@',
    authenticate: '@',
    user: '<?',
    ctrl: '=?'
  }
})

DefineRouteController.$inject = ['$window', '$rootScope', 'authService']

function DefineRouteController ($window, $rootScope, authService) {
  let scope = this

  scope.$onInit = () => {
    checkRoute()

    $rootScope.$on(EVENT_NAME, (event, data) => {
      scope.data = data
      checkRoute()
    })

    if (scope.authenticate) {
      $rootScope.$on('login', () => checkRoute())
    }
  }

  function checkRoute () {
    if (scope.path !== $window.location.pathname) {
      const re = new RegExp('^' + scope.path + '$')
      if (!re.test($window.location.pathname)) {
        scope.show = false
        return
      } else {
        scope.params = re.exec($window.location.pathname).slice(1)
      }
    }

    if (scope.authenticate) {
      if (authService.isLoggedIn()) {
        scope.show = true
      } else {
        scope.show = false
        $rootScope.$emit('unauthorizedView', scope.path)
      }
    } else {
      scope.show = true
    }
  }
}

app.component('linkTo', {
  template: `
    <a href="#" ng-click="scope.handleClick($event)" class="{{scope.className}}" ng-class="{'is-active': scope.isActive === 'true'}"><span ng-transclude></span></a>
  `,
  transclude: true,
  controller: LinkToController,
  controllerAs: 'scope',
  bindings: {
    path: '@',
    data: '<?',
    isActive: '@?',
    className: '@?'
  }
})

LinkToController.$inject = ['$window', '$rootScope']

function LinkToController ($window, $rootScope) {
  let scope = this

  scope.$onInit = () => console.log('LinkTo', scope)

  $window.onpopstate = () => $rootScope.$emit(EVENT_NAME)

  scope.handleClick = (ev) => {
    ev.preventDefault()
    if (scope.path !== $window.location.pathname) {
      $window.history.pushState({}, '', scope.path)
      $rootScope.$emit(EVENT_NAME, scope.data)
    }
  }
}
