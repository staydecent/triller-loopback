import app from '../app'

app.service('authService', authService)

authService.$inject = ['$q', 'LoopBackAuth', 'Person']

function authService ($q, LoopBackAuth, Person) {
  let currentUserId = null

  return {login, logout, register, isLoggedIn, currentUser}

  function login (email, password) {
    return $q((resolve, reject) =>
      Person
        .login({rememberMe: true}, {email, password})
        .$promise
        .then((resp) => {
          currentUserId = resp.user.id
          currentUser().then((user) => resolve(user))
        })
        .catch((err) => reject(err))
    )
  }

  function logout () {
    return Person
      .logout()
      .$promise
      .then(() => {
        currentUserId = null
        console.log('logout', LoopBackAuth)
      })
  }

  function register (email, password) {
    return Person
      .create({
        email: email,
        password: password
      })
      .$promise
  }

  function isLoggedIn () {
    if (currentUserId) {
      return currentUserId
    } else {
      if (LoopBackAuth.accessTokenId && LoopBackAuth.currentUserId) {
        currentUserId = LoopBackAuth.currentUserId
        return currentUserId
      }
    }
    return false
  }

  function currentUser () {
    return $q((resolve, reject) => {
      if (isLoggedIn()) {
        Person
          .findById({id: currentUserId})
          .$promise
          .then((resp) => resolve(resp))
      } else {
        reject(undefined)
      }
    })
  }
}
