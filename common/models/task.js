'use strict'

module.exports = function (Task) {
  Task.beforeRemote('create', function (ctx, user, next) {
    if (!ctx.isNewInstance) {
      next()
    } else {
      Task.count(function (err, count) {
        if (err) throw err
        ctx.args.data.order = count + 1
        next()
      })
    }
  })
}
