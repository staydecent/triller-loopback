import buble from 'rollup-plugin-buble'
import resolve from 'rollup-plugin-node-resolve'
import cjs from 'rollup-plugin-commonjs'
import uglify from 'rollup-plugin-uglify'

export default {
  entry: 'client/src/main.js',
  format: 'iife',
  moduleName: 'Triller',
  plugins: [
    buble(),
    resolve(),
    cjs(),
    uglify()
  ],
  dest: 'client/build/bundle.js'
}
